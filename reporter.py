import configparser
import argparse
import jobs.report
import jobs.squad
import os
import sys
from requests.compat import urljoin
from squad_client.core.api import SquadApi

parser = argparse.ArgumentParser()
parser.add_argument("report", help="Report type to create")
args = parser.parse_args()

# TODO: arg
SquadApi.configure(url=os.environ["QA_SERVER"])

# TODO: arg
# import requests_cache
# requests_cache.install_cache("sanity")

config = configparser.ConfigParser()
config.read("report.ini")

# TODO: validate
group, project, build, build2 = jobs.squad.parse_build_string(
    f"{os.environ['QA_TEAM']},{os.environ['SQUAD_PROJECT']},{os.environ['GIT_DESCRIBE']}"
)

# TODO: throw exception, log, exit
if not build.finished:
    sys.exit(f"Build {build.version} has not yet finished")

build_details_url = urljoin(
    SquadApi.url, "%s/%s/build/%s" % (group.slug, project.slug, build.version)
)

environments = jobs.squad.get_environments(project)

suite_slugs = config["sanity"]["suites"].split(",")
suites = jobs.squad.get_suites(project, suite_slugs)

# TODO: build_data.py
summary, fails, skips = jobs.squad.get_summary(build, environments, suites)

regressions, fixes = jobs.squad.parse_changes(
    jobs.squad.get_changes(project, build2.id, build.id), environments, suites
)

# te = jobs.report.build_template_env(config["sanity"]["templates"])
te = jobs.report.build_template_env(f"templates/{args.report}")

# TODO: url
# TODO: subject
# TODO: prev build
body = te.get_template("body.txt.jinja").render(
    build=build,
    build2=build2,
    build_details_url=build_details_url,
    environments=environments,
    suites=suites,
    summary=summary,
    regressions=regressions,
    fixes=fixes,
    fails=fails,
    skips=skips,
)

with open("report.txt", "w") as f:
    f.write(body)
